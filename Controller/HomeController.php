<?php

namespace Smle\PanBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class HomeController extends Controller
{
    public function indexUserAction($user)
    {
        return $this->render('SmlePanBundle:Home:userHome.html.twig', array(
        ));
    }
        
    public function indexAdminAction()
    {
        //die('ici');
        $em = $this->getDoctrine()->getManager();
		/* Test mailer : use gmail smtp
		$message = \Swift_Message::newInstance()
			->setSubject('Test')
			->setFrom('info@smle.fr')
			->setTo('jmarc.porcherot@free.fr')
			->setBody($this->renderView('SmlePanBundle:Home:email.txt.twig'))
		;
		$this->get('mailer')->send($message);
		* 
		* 
        $dateStart = clone $today;
        $dateStart->modify('-26 Days');
        $panierOrders = $em->getRepository('SmlePanBundle:PanierOrder')->findAfterDate($dateStart);
        
        $tAmaps = array();
        foreach($panierOrders as $po) {
            $tAmaps[$po
        }

        */
        
        //Define monday one week before (last week)
        $date = new \DateTime('today');
        $date->modify('-'.($date->format('w')+13).' Days');

        $tData = array();

        //Get all the paniers per amap
        $conn = $this->container->get('database_connection');
        $sql = "SELECT * FROM view_pan_amap_panier";
        $results = $conn->query($sql);
        
        $tPaniers = array();
        foreach($results as $row) {
            $tPaniers[$row['panier_id']] = $row['panier_name'];
            $tAmapPaniers[$row['amap_id']][$row['panier_id']] = true;
        }
        
        $amaps = $em->getRepository('SmlePanBundle:Amap')->findAll();
        
        $week = 0;
        do {
            $tAmaps = array();
            
            foreach($amaps as $amap) {
                $panierOrders = $em->getRepository('SmlePanBundle:PanierOrder')->findFromAmapByDate($date->format('Y-m-d'), $amap->getId());
                // Préparer le tableau vide
                if(count($panierOrders)) {
/*
echo "<pre>";echo $po->getPanier()->getId().",".$amap->getId().$week;echo "</pre>";
*/
                    
                    foreach($panierOrders as $po) {
                        $tAmaps[$amap->getId()][$po->getPanier()->getId()] = $po;
                    }
                }
            }
            $tData[$date->format('W')] = array(
                'date' => clone $date,
                'tAmapPo' => $tAmaps,
                'recap' => array()
                );
            
            //Recap for all AMAPs this week
            $conn = $this->container->get('database_connection');

            $this->getQtyRecap($conn, $date, $tData[$date->format('W')]['recap']);
/*
echo "<pre>";print_r(isset($tData[$date->format('W')]['tAmapPo']));die(); 
*/ 
              
            $date->modify('+7 Days');
            $week++;
        }while($week < 4);

        return $this->render('SmlePanBundle:Home:adminHome.html.twig', array(
            't_data' => $tData,
            't_paniers' => $tPaniers,
            't_amap_paniers' => $tAmapPaniers,
            'amaps' => $amaps, 
        ));
    }
    
    /**
     * Confirmation registration 
     * 
    */
    public function confirmedAction($user)
    {
        return $this->render('SmlePanBundle:Home:confirmed.html.twig', array(
            'user' => $user 
        ));
    }
    
    /**
     * Build 
     * 
    */
    private function getQtyRecap($connexion, \DateTime $date, &$tResult, $complete = false)
    {
        $em = $this->getDoctrine()->getManager();
        
        $weekProduction = $em->getRepository('SmlePanBundle:WeekProduction')->findByDate($date);
        
        if($weekProduction) {
            foreach($weekProduction[0]->getProductUnits() as $pu) {
                $productId = $pu->getProductPrice()->getId();
                $tResult[$productId]['prod'] = $pu->getQuantity();
                $tResult[$productId]['name'] = $pu->getProductPrice()->getProductUnit()->getProduct()->getLabel();
                $tResult[$productId]['unit'] = $pu->getProductPrice()->getProductUnit()->getUnit()->getLabel();
                $tResult[$productId]['qtyp'] = 0;
                $tResult[$productId]['pricep'] = 0;
                $tResult[$productId]['qtym'] = 0;
                $tResult[$productId]['pricem'] = 0;
            }
        }

        $sql = "SELECT * FROM pan_view_panier_recap ap WHERE po_date = '".$date->format('Y-m-d')."'";
        $results = $connexion->query($sql);
        
        foreach($results as $row) {
            $tResult[$row['product_id']]['qtyp'] = $row['totq'];
            $tResult[$row['product_id']]['pricep'] = $row['totp'];
            $tResult[$row['product_id']]['name'] = $row['product_name'];
            $tResult[$row['product_id']]['unit'] = $row['unit_name'];
            $tResult[$row['product_id']]['qtym'] = 0;
            $tResult[$row['product_id']]['pricem'] = 0;
            if(!isset($tResult[$row['product_id']]['prod'])) $tResult[$row['product_id']]['prod'] = 0;
        }
        
        if($complete) {
            $sql = "
                SELECT 
                    DATE_TRUNC('week', c.date_livraison) AS semaine,
                    cd.fk_product, 
                    p.label, 
                    CEIL(SUM(cd.qty)) AS qty, 
                    CEIL(SUM(cd.total_ttc)) AS total_ttc 
                FROM 
                    (llx_commande c INNER JOIN llx_commandedet cd ON c.rowid = cd.fk_commande)
                    INNER JOIN llx_product p ON cd.fk_product = p.rowid 
                WHERE TO_CHAR(c.date_livraison, 'IW') = '".$date->format('W')."'
                GROUP BY semaine, cd.fk_product, p.label
                ORDER BY semaine, cd.fk_product
                ";
            $results = $connexion->query($sql);
            
            foreach($results as $row) {
                if(!isset($tResult[$row['fk_product']])) {
                    $tResult[$row['fk_product']] = array(
                        'name' => $row['label'],
                        'qtyp' => 0,
                        'pricep' => 0);
                }
                $tResult[$row['fk_product']]['qtym'] = $row['qty'];
                $tResult[$row['fk_product']]['pricem'] = $row['total_ttc'];
                
            }
        }
        
        return true;
    }

    /**
     * display a recap of all quantity for a week.
     *
     */
    public function printQtyRecapAction($date)
    {
        $date = new \DateTime($date);

        $conn = $this->container->get('database_connection');
        
        $tData['recap'] = array();
        
        $this->getQtyRecap($conn, $date, $tData['recap'], true);
        
        return $this->render('SmlePanBundle:Home:qtyrecap.html.twig', array(
            'week' => $tData,
            'date' => $date
        ));
        
    }

    /**
     * Print a pdf recap of all quantity for a week.
     *
     */
    public function printQtyRecapToPdfAction($date)
    {
        $pageUrl = $this->generateUrl('home_printqtyrecapbeforepdf', array(
            'date' => $date,
            'media' => 'print'
            ), true);

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl, array(
                'orientation'=>'Landscape',
                'print-media-type' => true)
                ),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="qtyrecap-'.$date.'pdf"'
                )
            );
        
    }
}
