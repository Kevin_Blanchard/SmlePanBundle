<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;

use Smle\PanBundle\Entity\ProductPrice;
use Smle\PanBundle\Form\ProductPriceType;

/**
 * ProductPrice controller.
 *
 */
class ProductPriceController extends Controller
{
    /**
     * Lists all ProductPrice entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:ProductPrice')->findAll();

        return $this->render('SmlePanBundle:ProductPrice:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a ProductPrice entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:ProductPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductPrice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:ProductPrice:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new ProductPrice entity.
     *
     */
    public function newAction()
    {
        $request = $this->container->get('request');

        $entity = new ProductPrice();
        
        if($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getManager();

            $productUnitId = $request->request->get('productUnit');
            $productUnit = $em->getRepository('SmlePanBundle:ProductUnit')->find($productUnitId);
            if (!$productUnit) {
                throw $this->createNotFoundException('Unable to find ProductUnit entity.');
            }
            $date = new \DateTime($request->request->get('date'));
            
            $entity->setProductUnit($productUnit);
            $entity->setDStart($date);

            $form = $this->createForm(new ProductPriceType(), $entity);

            return $this->render('SmlePanBundle:ProductPrice:new.html.twig', array(
                'entity'      => $entity,
                'form'   => $form->createView(),
                'dateRef' => $request->request->get('dateRef'),
                'rowstart' => $request->request->get('rowstart')
            ));
        }
    }

    /**
     * Insert a new ProductPrice entity.
     *
     */
    private function insert($entity, $new=true)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Environment
        $previousEntity = $em->getRepository('SmlePanBundle:ProductPrice')->findPrevious($entity);
        $nextEntity = $em->getRepository('SmlePanBundle:ProductPrice')->findNext($entity);
        
        $dateEnd = clone $entity->getDStart();
        //Define date end
        if($entity->getPrice())
        {
            if($nextEntity)
            {
                $nDateEnd = clone $nextEntity->getDStart();
                $entity->setDEnd($nDateEnd->modify('-1 Day'));
            }
            else if($new)
            {
                $entity->setDEnd($dateEnd->modify('+1 Year'));
            }
        }
        else
        {
            $entity->setDEnd($dateEnd->modify('+1 Day'));
        }

        //Update date end previous
        if($previousEntity && $previousEntity->getDEnd() >= $entity->getDStart())
        {
            $pDateEnd = clone $entity->getDStart();
            $previousEntity->setDEnd($pDateEnd->modify('-1 Day'));
            $em->persist($previousEntity);
        }
        
        if($entity->getPrice() > 0)
            $em->persist($entity);

        $em->flush();
        
        return;
    }

    /**
     * Remove a ProductPrice entity.
     *
     */
    private function remove($entity)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Environment
        $previousEntity = $em->getRepository('SmlePanBundle:ProductPrice')->findPrevious($entity);
        
        $em->remove($entity);

        $em->flush();
        
        return;
    }

    /**
     * Creates a new ProductPrice entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new ProductPrice();
        $form = $this->createForm(new ProductPriceType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $this->insert($entity);
            return $this->redirect($this->generateUrl('product_date_all', array(
                        'ddeb' => $request->request->get('date_ref'),
                        'rowstart' => $request->request->get('rowstart')
                        )));
        }

        return $this->render('SmlePanBundle:ProductPrice:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ProductPrice entity.
     *
     */
    public function editAction()
    {
        $request = $this->container->get('request');

        if($request->isXmlHttpRequest())
        {
            $id = $request->request->get('id');
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('SmlePanBundle:ProductPrice')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProductPrice entity.');
            }
            $panierAffects = $em->createQuery("SELECT count(popu) 
                    FROM SmlePanBundle:PanierOrderProductUnit popu INNER JOIN popu.productPrice pp
                    WHERE pp.id = :id")
                    ->setParameter('id', $entity->getId())
                    ->getSingleScalarResult();
/*            
            $result = $panierAffects->getResult();
            $result = 1;
return new Response($panierAffects);
*/

            $editForm = $this->createForm(new ProductPriceType(), $entity);
            $deleteForm = $this->createDeleteForm($id);

            return $this->render('SmlePanBundle:ProductPrice:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'dateRef' => $request->request->get('dateRef'),
                'rowstart' => $request->request->get('rowstart'),
                'removable' => $panierAffects
            ));
        }
        return new Response('Erreur');
    }

    /**
     * Edits an existing ProductPrice entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:ProductPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductPrice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ProductPriceType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $this->insert($entity, false);
            /*
            $em->persist($entity);
            $em->flush();
            */

            //return $this->redirect($this->generateUrl('productprice_edit', array('id' => $id)));
        }

        return $this->redirect($this->generateUrl('product_date_all', array(
                    'ddeb' => $request->request->get('date_ref'),
                    'rowstart' => $request->request->get('rowstart')
                    )));
    }

    /**
     * Deletes a ProductPrice entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        //if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SmlePanBundle:ProductPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductPrice entity.');
        }

        $this->remove($entity);
        //}

        return $this->redirect($this->generateUrl('product_date_all', array(
                    'ddeb' => $request->request->get('date_ref'),
                    'rowstart' => $request->request->get('rowstart')
                    )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
