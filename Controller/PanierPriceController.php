<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\PanierPrice;
use Smle\PanBundle\Form\PanierPriceType;

use Symfony\Component\HttpFoundation\Response;

/**
 * PanierPrice controller.
 *
 */
class PanierPriceController extends Controller
{
    /**
     * Lists all PanierPrice entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:PanierPrice')->findAll();

        return $this->render('SmlePanBundle:PanierPrice:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a PanierPrice entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierPrice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:PanierPrice:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new PanierPrice entity.
     *
     */
    public function newAction()
    {
        $request = $this->container->get('request');
        $entity = new PanierPrice();

        if($request->isXmlHttpRequest())
        {
            $panierId = $request->request->get('panierId');
            $em = $this->getDoctrine()->getManager();

            $panier = $em->getRepository('SmlePanBundle:Panier')->find($panierId);

            if (!$panier) {
                throw $this->createNotFoundException('Unable to find ProductPrice entity.');
            }
            
            $entity->setPanier($panier);

            $form = $this->createForm(new PanierPriceType(), $entity);

            return $this->render('SmlePanBundle:PanierPrice:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }
        return new Response('Erreur');
    }

    /**
     * Creates a new PanierPrice entity.
     *
     */
    public function updateLast($em, $entity)
    {
        $lastEntity = $em->getRepository('SmlePanBundle:PanierPrice')
            ->findBy(array(
                'panier' => $entity->getPanier()->getId(),
                'date_end' => null
                )); 
        
        if($lastEntity) {
            $dateEnd = clone $entity->getDateStart();
            return $lastEntity[0]->setDateEnd($dateEnd->modify('-1 Day'));
        }
        
        return null;
        
    }

    /**
     * Creates a new PanierPrice entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new PanierPrice();
        $form = $this->createForm(new PanierPriceType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            if(!$entity->getDateStart()) $entity->setDateStart(new \DateTime('today'));
            
            $em = $this->getDoctrine()->getManager();
            // Set a date_end to the previous entity
            if($lastEntity = $this->updateLast($em, $entity)) {
                if($lastEntity->getDateStart() >= $entity->getDateStart())
                    return $this->redirect($this->generateUrl('panier'));
                $em->persist($lastEntity);
            }
            
            $em->persist($entity);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('panier'));
    }

    /**
     * Displays a form to edit an existing PanierPrice entity.
     *
     */
    public function editAction()
    {
        $request = $this->container->get('request');

        if($request->isXmlHttpRequest())
        {
            $id = $request->request->get('id');
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('SmlePanBundle:PanierPrice')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProductPrice entity.');
            }

            $editForm = $this->createForm(new PanierPriceType(), $entity);
        
            $deleteForm = $this->createDeleteForm($id);

            return $this->render('SmlePanBundle:PanierPrice:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        return new Response('Erreur');
    }

    /**
     * Edits an existing PanierPrice entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierPrice')->find($id);

        if (!$entity) {
            $entity = new PanierPrice;
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PanierPriceType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panier'));
    }

    /**
     * Deletes a PanierPrice entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:PanierPrice')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PanierPrice entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panierprice'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
