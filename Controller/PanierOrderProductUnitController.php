<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\PanierOrderProductUnit;
use Smle\PanBundle\Form\PanierOrderProductUnitType;

/**
 * PanierOrderProductUnit controller.
 *
 */
class PanierOrderProductUnitController extends Controller
{
    /**
     * Lists all PanierOrderProductUnit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:PanierOrderProductUnit')->findAll();

        return $this->render('SmlePanBundle:PanierOrderProductUnit:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a PanierOrderProductUnit entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrderProductUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrderProductUnit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:PanierOrderProductUnit:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new PanierOrderProductUnit entity.
     *
     */
    public function newAction()
    {
        $entity = new PanierOrderProductUnit();
        $form   = $this->createForm(new PanierOrderProductUnitType(), $entity);

        return $this->render('SmlePanBundle:PanierOrderProductUnit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new PanierOrderProductUnit entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new PanierOrderProductUnit();
        $form = $this->createForm(new PanierOrderProductUnitType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panierorderproductunit_show', array('id' => $entity->getId())));
        }

        return $this->render('SmlePanBundle:PanierOrderProductUnit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PanierOrderProductUnit entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrderProductUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrderProductUnit entity.');
        }

        $editForm = $this->createForm(new PanierOrderProductUnitType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:PanierOrderProductUnit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing PanierOrderProductUnit entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrderProductUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrderProductUnit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PanierOrderProductUnitType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panierorderproductunit_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:PanierOrderProductUnit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PanierOrderProductUnit entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:PanierOrderProductUnit')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PanierOrderProductUnit entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panierorderproductunit'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
