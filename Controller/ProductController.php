<?php

namespace Smle\PanBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\Product;
use Smle\PanBundle\Form\ProductType;
use Smle\PanBundle\Entity\ProductUnit;
use Smle\PanBundle\Form\ProductUnitNewType;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

/**
 * Product controller.
 *
 */
class ProductController extends Controller
{
    /**
     * Lists all Product entities.
     *
     */
    public function showAllAction()
    {
        $this->updateAllAction();
        $date = new \DateTime(date('Y').'-01-01');
        $date->modify('-'.($date->format('w')-1).' Days');
        return $this->showAllDateUnitAction(new \DateTime($date->format('Y-m-d')), 0, 0);
    }

    /**
     * Lists all Product entities, by Unit.
     *
     */
    public function showAllUnitAction($unit)
    {
        $date = new \DateTime(date('Y').'-01-01');
        $date->modify('-'.($date->format('w')-1).' Days');
        return $this->showAllDateUnitAction(new \DateTime($date->format('Y-m-d')), 0, $unit);
    }

    /**
     * Price history of a Product entity.
     *
     */
    public function showHistoryAction($id, $year, $unit)
    {
        $year -= 2; // Two years before
        $em = $this->getDoctrine()->getManager();
        
        $entities = array();

        $units = array();
        
        for($i=0; $i < 4; $i++) // Until one year later
        {
            $date = new \DateTime(($year + $i).'-01-01');
            $date->modify('-'.($date->format('w')-1).' Days');

            $dfin = clone $date;
            
            $dfin->add(new \DateInterval('P1Y'));
            
            $units = $em->getRepository('SmlePanBundle:ProductUnit')->findByProduct($id);
            
            $entities[$date->format('Y-m-d')] = $unit ? 
                $em->getRepository('SmlePanBundle:ProductUnit')->findBy(array('product' => $id, 'unit' => $unit)) :
                $units;
                
            foreach($entities[$date->format('Y-m-d')] as &$entity)
            {
                $this->hydrateSetPrices($em, $entity, $date, $dfin);
            }
        }
        
        
        return $this->render('SmlePanBundle:Product:showHistory.html.twig', array(
            'entities' => $entities,
            'dateRef' => $date,
            'product_id' => $id,
            'units' => $units,
            'u' => $unit
        ));
    }

    /**
     * Hydrate with ProductPrices.
     *
     */
    private function hydrateSetPrices($em, &$productUnit, \DateTime $ddeb, \DateTime $dfin)
    {
        $productPrices = $em->getRepository('SmlePanBundle:ProductPrice')->findByDates(
                $productUnit,
                $ddeb,
                $dfin
            );

        foreach($productPrices as $productPrice)
        {
            $productUnit->addProductPrice($productPrice);
        }
        
        return;
    }

    /**
     * Prepare the display Product entities by unit and by date.
     *
     */
    public function showAllDateUnitAction(\DateTime $ddeb, $rowstart, $unit)
    {
        $dfin = clone $ddeb;
        $dfin->add(new \DateInterval('P1Y'));
        
        return $this->render('SmlePanBundle:Product:showAll.html.twig', array(
            'dateRef' => $ddeb,
            'rowstart' => $rowstart,
            'u' => $unit
        ));
    }

    /**
     * Lists all Product entities by unit and by date.
     *
     */
    public function showAllGetDataAction() //ddeb, unit, screenW, screenH
    {
        $request = $this->container->get('request');

        if($request->isXmlHttpRequest())
        {
            //Define displaying parameters
            $coeff = ($request->request->get('coeff'));
            $nbRows = ($request->request->get('nbrows'));
            $unit = ($request->request->get('unit'));
            $ddeb = new \DateTime($request->request->get('ddeb'));
            $dfin = clone $ddeb;
            $dfin->add(new \DateInterval('P1Y'));
            
            $em = $this->getDoctrine()->getManager();

            $entities = $unit ? 
                $em->getRepository('SmlePanBundle:ProductUnit')->findByUnit($request->request->get('unit')) :
                $em->getRepository('SmlePanBundle:ProductUnit')->findAll();
            
            $units = $em->getRepository('SmlePanBundle:Unit')->findAll();
            
            foreach($entities as &$entity)
            {
                $this->hydrateSetPrices($em, $entity, $ddeb, $dfin);
            }
            
            return $this->render('SmlePanBundle:Product:showAllGetData.html.twig', array(
                'entities' => $entities,
                'dateRef' => $ddeb,
                'units' => $units,
                'rowstart' => (int)$request->request->get('rowstart'),
                'u' => $unit,
                'nb_rows' => $nbRows,
                'coeff' => $coeff
            ));
        }
        return new Response('aïe');
/*
*/
    }

    /**
     * Finds and displays a Product entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        
        /*
        //$form = $this->createForm(new ProductType(), $entity);
        $form = $this->createFormBuilder($entity)
                ->add('productUnits', 'collection',
                    array(
                        'type' => new ProductUnitNewType(),
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false
                    ))
                ->getForm();
        */
        
        return $this->render('SmlePanBundle:Product:show.html.twig', array(
            'entity'      => $entity,
            //'form'        => $form->createView()
        ));
    }

    /**
     * Adds a price.
     *
     */
    public function addUnitAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $form = $this->createForm(new ProductType(), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
            foreach($entity->getProductUnits() as $unit) {
                $unit->setProduct($entity);
                $em->persist($unit);
            }
/*
            $em->persist($entity);
*/
            $em->flush();
        }

        return $this->render('SmlePanBundle:Product:show.html.twig', array(
            'entity'      => $entity,
            'form'        => $form->createView()
        ));

    }

    /**
     * Add a default unit for all product
     *
     */
    public function updateAllAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:Product')->findAll();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $defaultUnit = $em->getRepository('SmlePanBundle:Unit')->find(1);
        
        foreach($entities as $entity)
        {
            if(!count($entity->getProductUnits()))
            {
                $productUnit = new ProductUnit;
                $productUnit->setUnit($defaultUnit);
                $productUnit->setWeight(1);
                $productUnit->setProduct($entity);
                
                $em->persist($productUnit);
            }
        }

        $em->flush();

        return $this->redirect($this->generateUrl('product'));

    }

}
