<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PanierOrderAdherentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('panierAdherents', 'collection', array(
                'type' => new PanierAdherentWithType(),
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => true
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\PanierOrder'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_panierorderadherenttype';
    }
}
