<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AmapDeliveryDayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_start', 'date', array('widget' => 'single_text'))
            ->add('day', 'choice', array('choices' => array(
                '1' => 'lundi',
                '2' => 'mardi',
                '3' => 'mercredi',
                '4' => 'jeudi',
                '5' => 'vendredi',
                '6' => 'samedi',
                '0' => 'dimanche',
                )))
            ->add('amap', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\Amap',
                'property' => 'name'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\AmapDeliveryDay'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_amapdeliverydaytype';
    }
}
