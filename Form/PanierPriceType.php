<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PanierPriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('date_start', 'date', array('widget' => 'single_text'))
            ->add('date_end', null, array('widget' => 'single_text', 'attr' => array('class' => 'hidden')))
            ->add('panier', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\Panier',
                'property' => 'name',
                'attr' => array('class' => 'hidden')
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\PanierPrice'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_panierpricetype';
    }
}
