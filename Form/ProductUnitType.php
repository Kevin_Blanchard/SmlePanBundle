<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductUnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weight')
            ->add('unit', new UnitType())
            /*
            ->add('product', new ProductType())
            ->add('productPrices', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\ProductPrice',
                'property' => 'id'
                ))
            */
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\ProductUnit'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_productunittype';
    }
}
