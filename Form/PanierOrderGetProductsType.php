<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PanierOrderGetProductsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productUnits', 'collection', array(
                'type' => new PanierOrderProductUnitType(),
                'allow_add' => 'true',
                'allow_delete' => 'true',
                'by_reference' => false)
                )
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\PanierOrder'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_panierordergetproductstype';
    }
}
