<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productUnit', 'entity', array( 'class' => 'Smle\PanBundle\Entity\ProductUnit', 'property' => 'id'))
            ->add('id', 'hidden')
            ->add('dStart', 'date', array('widget' => 'single_text'))
            ->add('dEnd', 'date', array('widget' => 'single_text'))
            ->add('price')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\ProductPrice'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_productpricetype';
    }
}
