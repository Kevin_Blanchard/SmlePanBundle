<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductAddUnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productUnits', 'collection', 
				array(
					'type' => new ProductUnitNewType(),
					'allow_add' => 'true',
					'allow_delete' => 'true',
					'by_reference' => false
                    )
				) 
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\Product'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_productaddunittype';
    }
}
