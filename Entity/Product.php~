<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\Product
 *
 * @ORM\Table("view_pan_product")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\ProductRepository")
 */
class Product
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $ref
     *
     * @ORM\Column(name="ref", type="string", length=32)
     */
    private $ref;

    /**
     * @var string $label
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price_ttc", type="decimal", precision=10, scale=2)
     */
    private $price;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\PanierProduct", mappedBy="product")
	 */
	private $panierProducts;

	/**
	 */
	private $productPrices;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\ProductUnit", mappedBy="product")
	 */
	private $productUnits;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Product
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->panierProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productPrices = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add panierProducts
     *
     * @param Smle\PanBundle\Entity\PanierProduct $panierProducts
     * @return Product
     */
    public function addPanierProduct(\Smle\PanBundle\Entity\PanierProduct $panierProducts)
    {
        $this->panierProducts[] = $panierProducts;
    
        return $this;
    }

    /**
     * Remove panierProducts
     *
     * @param Smle\PanBundle\Entity\PanierProduct $panierProducts
     */
    public function removePanierProduct(\Smle\PanBundle\Entity\PanierProduct $panierProducts)
    {
        $this->panierProducts->removeElement($panierProducts);
    }

    /**
     * Get panierProducts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPanierProducts()
    {
        return $this->panierProducts;
    }

    /**
     * Add productPrices
     *
     * @param Smle\PanBundle\Entity\ProductPrice $productPrices
     * @return Product
     */
    public function addProductPrice(\Smle\PanBundle\Entity\ProductPrice $productPrices)
    {
        $this->productPrices[] = $productPrices;
    
        return $this;
    }

    /**
     * Remove productPrices
     *
     * @param Smle\PanBundle\Entity\ProductPrice $productPrices
     */
    public function removeProductPrice(\Smle\PanBundle\Entity\ProductPrice $productPrices)
    {
        $this->productPrices->removeElement($productPrices);
    }

    /**
     * Get productPrices
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProductPrices()
    {
        return $this->productPrices;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Product
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    
        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     * @return Product
     */
    public function addProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnits)
    {
        $this->productUnits[] = $productUnits;
    
        return $this;
    }

    /**
     * Remove productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     */
    public function removeProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnits)
    {
        $this->productUnits->removeElement($productUnits);
    }

    /**
     * Get productUnits
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProductUnits()
    {
        return $this->productUnits;
    }
}