<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\Amap
 *
 * @ORM\Table("view_pan_amap")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\AmapRepository")
 */
 
//virtual_pan_amap //
class Amap
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string $cp
     *
     * @ORM\Column(name="cp", type="string", length=10)
     */
    private $cp;

    /**
     * @var string $city
     *
     * @ORM\Column(name="city", type="string", length=100)
     */
    private $city;

    /**
     * @var string $phone
     *
     * @ORM\Column(name="phone", type="string", length=14)
     */
    private $phone;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var bool $contacts
     */
    private $contacts;

	/**
	 */
    private $amapDeliveryDays;

	/**
	 */
    private $panierOrders;

    private $paniers;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contacts
     *
     * @param bool $contacts
     * @return Amap
     */
    public function setContacts($bool)
    {
        $this->contacts = $bool;
    
        return $this;
    }

    /**
     * Get contacts
     *
     * @return bool 
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Amap
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Amap
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Amap
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    
        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Amap
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Amap
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Amap
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->amapDeliveryDays = new \Doctrine\Common\Collections\ArrayCollection();
        $this->panierOrders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->paniers = array();
    }

    /**
     * Add amapDeliveryDays
     *
     * @param Smle\PanBundle\Entity\AmapDeliveryDay $amapDeliveryDays
     * @return Amap
     */
    public function addAmapDeliveryDay(\Smle\PanBundle\Entity\AmapDeliveryDay $amapDeliveryDays)
    {
        $this->amapDeliveryDays[] = $amapDeliveryDays;
    
        return $this;
    }

    /**
     * Remove amapDeliveryDays
     *
     * @param Smle\PanBundle\Entity\AmapDeliveryDay $amapDeliveryDays
     */
    public function removeAmapDeliveryDay(\Smle\PanBundle\Entity\AmapDeliveryDay $amapDeliveryDays)
    {
        $this->amapDeliveryDays->removeElement($amapDeliveryDays);
    }

    /**
     * Get amapDeliveryDays
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAmapDeliveryDays()
    {
        return $this->amapDeliveryDays;
    }

    /**
     * Add panierOrders
     *
     * @param \Smle\PanBundle\Entity\PanierOrder $panierOrders
     * @return Amap
     */
    public function addPanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrders)
    {
        $this->panierOrders[] = $panierOrders;
    
        return $this;
    }

    /**
     * Remove panierOrders
     *
     * @param \Smle\PanBundle\Entity\PanierOrder $panierOrders
     */
    public function removePanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrders)
    {
        $this->panierOrders->removeElement($panierOrders);
    }

    /**
     * Get panierOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPanierOrders()
    {
        return $this->panierOrders;
    }

    /**
     * Add panier
     *
     * @param $panierId
     * @return Amap
     */
    public function addPanier($panierId)
    {
        $this->paniers[] = $panierId;
    
        return $this;
    }

    /**
     * Get panier
     *
     * @return panierId 
     */
    public function getPanier($index)
    {
        return $this->paniers[$index];
    }

    /**
     * Get count(paniers)
     *
     * @return panierId 
     */
    public function getPaniersLength()
    {
        return count($this->paniers);
    }
}
