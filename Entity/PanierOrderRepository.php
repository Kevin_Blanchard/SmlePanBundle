<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * PanierOrderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PanierOrderRepository extends EntityRepository
{
    public function findAmapPaniers($date, $amapId)
    {
		$qb = $this->createQueryBuilder('po')
                        ->join('po.panierAdherents', 'pa')
                        ->join('pa.amapAdherent', 'aa')
                        ->where('po.date = :date')
                        ->andWhere('aa.amap = :amapId')
                        ->orderBy('po.panier')
                        ->setParameter('amapId', $amapId)
                        ->setParameter('date', $date)
                        ;
		return $qb->getQuery()->getResult();
    }

    /*
    public function findAfterDate($date)
    {
		$qb = $this->createQueryBuilder('po')
                        ->join('po.panierAdherents', 'pa')
                        ->join('pa.amapAdherent', 'aa')
                        ->where('po.date > :date')
                        ->orderBy('po.date DESC, po.panier')
                        ->setParameter('date', $date)
                        ;
		return $qb->getQuery()->getResult();
    }
    */

    public function findFromAmapByDate($date, $amapId)
    {
		$qb = $this->createQueryBuilder('po')
                        ->join('po.panierAdherents', 'pa')
                        ->join('pa.amapAdherent', 'aa')
                        ->where('po.date = :date')
                        ->andWhere('aa.amap = :amapId')
                        ->orderBy('po.date DESC, po.panier')
                        ->setParameter('amapId', $amapId)
                        ->setParameter('date', $date)
                        ;
		return $qb->getQuery()->getResult();
    }
    
    public function findAmapAfterDate($date, $amapId)
    {
		$qb = $this->createQueryBuilder('po')
                        ->join('po.panierAdherents', 'pa')
                        ->join('pa.amapAdherent', 'aa')
                        ->where('po.date > :date')
                        ->andWhere('aa.amap = :amapId')
                        ->orderBy('po.date DESC, po.panier')
                        ->setParameter('amapId', $amapId)
                        ->setParameter('date', $date)
                        ;
		return $qb->getQuery()->getResult();
    }
    
    public function findAmapLasts($amapId=0, $panierId=0, $excludeId=0, $limit=4)
    {
        $qb = $this->createQueryBuilder('po')
                        ->join('po.panierAdherents', 'pa')
                        ->join('pa.amapAdherent', 'aa')
                        ->where('po.id = po.id');
        if($amapId) $qb->andWhere('aa.amap = :amapId')->setParameter('amapId', $amapId);
        if($panierId) $qb->andWhere('po.panier = :panierId')->setParameter('panierId', $panierId);
        if($excludeId) $qb->andWhere('po.id != :exclude')->setParameter('exclude', $excludeId);
        $qb->orderBy('po.date DESC, po.panier');
        $qb->setFirstResult(0)->setMaxResults($limit < 11 ? $limit : 10);
        
		return new Paginator($qb, $fetchJoinCollection = true);
    }
}

