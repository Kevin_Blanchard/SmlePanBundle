<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ProductUnitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductUnitRepository extends EntityRepository
{
    function findCurrents($date)
    {
		$qb = $this->createQueryBuilder('pu');
        $qb->select('pu','pp')
                    ->join('Smle\PanBundle\Entity\ProductPrice', 'pp')
                    //->join('Smle\PanBundle\Entity\Unit', 'u')
                    ->where('pp.dStart <= :date')
                    ->andWhere('pp.dEnd >= :date')
                    ->andWhere('pp.price >0')
                    ->setParameter('date', $date->format('Y-m-d'));
				   ;
				   
		return $qb;
    }
}
