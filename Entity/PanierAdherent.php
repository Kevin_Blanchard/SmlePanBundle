<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\PanierAdherent
 *
 * @ORM\Table("pan_panier_adherent")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\PanierAdherentRepository")
 */
class PanierAdherent
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $date_start
     *
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $date_start;

    /**
     * @var \DateTime $date_end
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $date_end;

	/**
	 * @ORM\ManyToOne(targetEntity="Panier", inversedBy="panierAdherent")
	 * @ORM\JoinColumn(name="panier_id", referencedColumnName="id")
	 */
	private $panier;

	/**
	 * @ORM\ManyToOne(targetEntity="AmapAdherent", inversedBy="panierAdherent")
	 * @ORM\JoinColumn(name="amap_adherent_id", referencedColumnName="id")
	 */
	private $amapAdherent;

    /**
     * @ORM\ManyToMany(targetEntity="PanierOrder")
	 * @ORM\JoinColumn(name="panieradherent_id", referencedColumnName="id")
     * @ORM\JoinTable(name="pan_panier_order_adherent")
     */
    private $panierOrders;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date_start
     *
     * @param \DateTime $dateStart
     * @return PanierAdherent
     */
    public function setDateStart($dateStart)
    {
        $this->date_start = $dateStart;
    
        return $this;
    }

    /**
     * Get date_start
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * Set date_end
     *
     * @param \DateTime $dateEnd
     * @return PanierAdherent
     */
    public function setDateEnd($dateEnd)
    {
        $this->date_end = $dateEnd;
    
        return $this;
    }

    /**
     * Get date_end
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * Set panier
     *
     * @param Smle\PanBundle\Entity\Panier $panier
     * @return PanierAdherent
     */
    public function setPanier(\Smle\PanBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;
    
        return $this;
    }

    /**
     * Get panier
     *
     * @return Smle\PanBundle\Entity\Panier 
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * Set amapAdherent
     *
     * @param Smle\PanBundle\Entity\AmapAdherent $amapAdherent
     * @return PanierAdherent
     */
    public function setAmapAdherent(\Smle\PanBundle\Entity\AmapAdherent $amapAdherent = null)
    {
        $this->amapAdherent = $amapAdherent;
    
        return $this;
    }

    /**
     * Get amapAdherent
     *
     * @return Smle\PanBundle\Entity\AmapAdherent 
     */
    public function getAmapAdherent()
    {
        return $this->amapAdherent;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->panierOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add panierOrders
     *
     * @param \Smle\PanBundle\Entity\PanierOrder $panierOrders
     * @return PanierAdherent
     */
    public function addPanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrders)
    {
        $this->panierOrders[] = $panierOrders;
    
        return $this;
    }

    /**
     * Remove panierOrders
     *
     * @param \Smle\PanBundle\Entity\PanierOrder $panierOrders
     */
    public function removePanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrders)
    {
        $this->panierOrders->removeElement($panierOrders);
    }

    /**
     * Get panierOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPanierOrders()
    {
        return $this->panierOrders;
    }

    /**
     * Find panierOrder
     *
     * @return boolean
     */
    public function findPanierOrder(PanierOrder $panierOrder)
    {
        foreach($this->getPanierOrders() as $po) {
            if($po == $panierOrder) return true;
        }
        return false;
    }
}
